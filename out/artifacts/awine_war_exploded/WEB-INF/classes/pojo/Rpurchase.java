package pojo;

public class Rpurchase {
    private int purre_id;
    private int pur_rm_id;
    private int purre_d;
    private int p_containtax;
    private int purre_money;
    private int purre_refund;
    private int purre_per;
    private int purre_state;

    public int getPurre_id() {
        return purre_id;
    }

    public void setPurre_id(int purre_id) {
        this.purre_id = purre_id;
    }

    public int getPur_rm_id() {
        return pur_rm_id;
    }

    public void setPur_rm_id(int pur_rm_id) {
        this.pur_rm_id = pur_rm_id;
    }

    public int getPurre_d() {
        return purre_d;
    }

    public void setPurre_d(int purre_d) {
        this.purre_d = purre_d;
    }

    public int getP_containtax() {
        return p_containtax;
    }

    public void setP_containtax(int p_containtax) {
        this.p_containtax = p_containtax;
    }

    public int getPurre_money() {
        return purre_money;
    }

    public void setPurre_money(int purre_money) {
        this.purre_money = purre_money;
    }

    public int getPurre_refund() {
        return purre_refund;
    }

    public void setPurre_refund(int purre_refund) {
        this.purre_refund = purre_refund;
    }

    public int getPurre_per() {
        return purre_per;
    }

    public void setPurre_per(int purre_per) {
        this.purre_per = purre_per;
    }

    public int getPurre_state() {
        return purre_state;
    }

    public void setPurre_state(int purre_state) {
        this.purre_state = purre_state;
    }
}
