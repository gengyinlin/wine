<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2020-10-21
  Time: 18:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
String path=request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DCCTYPE HTML PUBLIC"-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">

    <title>My JSP 'index.jsp' starting page</title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->
    <script type="text/javascript" src="statics/js/jquery-1.8.3.min.js"></script>
  </head>
  <body>
  <c:if test="${empty pu}">
    <script>location='PageServlet';</script>
  </c:if>

  <ul>
    <c:forEach items="${pu.ulist }" var="ulist">
      <li>${ulist.username }---${ulist.userage }</li>
    </c:forEach>
  </ul>
  <a href="PageServlet?index=1">首页</a>
  <a href="PageServlet?index=${pu.index - 1 }">上一页</a>
  <a href="PageServlet?index=${pu.index + 1 }">下一页</a>
  <a href="PageServlet?index=${pu.totalPageCount }">尾页</a><br>
  当前第<span id="pi">${pu.index }</span>|共${pu.totalPageCount }
  <input type="text" id="ps">
  <input type="text" id="in">

  <script type="text/javascript">
    $(function (){
      $("#ps").blur(function(){
        location = 'PageServlet?index=' + this.value;
      });
      $("#in").blur(function(){
        location = 'PageServlet?index=' + $("#pi").text() + '&pageSize=' + this.value;
      });
    });
  </script>
  </body>
</html>
