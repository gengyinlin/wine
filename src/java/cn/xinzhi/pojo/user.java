package cn.xinzhi.pojo;

public class user {
    private int PI_id;
    private String PI_name;
    private int user_id;
    private String PI_sex;
    private String PI_education;
    private String PI_tel;
    private String PI_indentity;
    private int PI_address;
    private int r_id;


    public int getPI_id() {
        return PI_id;
    }

    public void setPI_id(int PI_id) {
        this.PI_id = PI_id;
    }

    public String getPI_name() {
        return PI_name;
    }

    public void setPI_name(String PI_name) {
        this.PI_name = PI_name;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPI_sex() {
        return PI_sex;
    }

    public void setPI_sex(String PI_sex) {
        this.PI_sex = PI_sex;
    }

    public String getPI_education() {
        return PI_education;
    }

    public void setPI_education(String PI_education) {
        this.PI_education = PI_education;
    }

    public String getPI_tel() {
        return PI_tel;
    }

    public void setPI_tel(String PI_tel) {
        this.PI_tel = PI_tel;
    }

    public String getPI_indentity() {
        return PI_indentity;
    }

    public void setPI_indentity(String PI_indentity) {
        this.PI_indentity = PI_indentity;
    }

    public int getPI_address() {
        return PI_address;
    }

    public void setPI_address(int PI_address) {
        this.PI_address = PI_address;
    }

    public int getR_id() {
        return r_id;
    }

    public void setR_id(int r_id) {
        this.r_id = r_id;
    }
}
