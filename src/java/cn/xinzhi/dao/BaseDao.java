package cn.xinzhi.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


public class BaseDao {

    public static Connection getConnection(){
        Connection conn =null ;
        Context ctx;
        try {
            ctx = new InitialContext();
            DataSource ds = (DataSource)ctx.lookup("java:comp/env/jdbc/news");
            conn = (Connection) ds.getConnection();
        }catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }catch(NamingException namingException){
            namingException.printStackTrace();
        }

        return conn;
    }

    public static void closeALL(ResultSet rs,Statement stat , Connection conn){
        try {
            if(rs!=null)
                rs.close();
            if(stat!=null)
                stat.close();
            if(conn!=null)
                conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
